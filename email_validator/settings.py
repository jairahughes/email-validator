# Copyright (c) Alessio Parma <alessio.parma@gmail.com>. All rights reserved.
#
# Licensed under the MIT License. See LICENSE file in the project root for full license information.

import datetime
from typing import Optional, Set, Tuple
from uuid import UUID

from pydantic import BaseSettings
from pydantic.fields import Field
from pydantic.main import BaseModel


class ApiKey(BaseModel):
    name: str = Field(description="Descriptive API key name")
    value: str = Field(description="API key value")
    expires_at: Optional[datetime.datetime] = Field(
        alias="expiresAt", description="Optional API key expiration timestamp"
    )

    class Config:
        # Following configuration generates an hashable model,
        # which is required in order to create a set of API keys.
        allow_mutation = False
        frozen = True


class SecuritySettings(BaseSettings):
    enabled: bool = Field(
        True,
        description="Enables or disables all security checks,"
        " such as API key and JWT token validations."
        " It can be set to false in order to perform quick local tests,"
        " but it is highly recommended to set it to true in production environments."
        " Default value is True.",
    )
    api_keys: Set[ApiKey] = Field(
        set(),
        description="API keys which could be used to perform security checks."
        " Each API key is described by three properties:"
        " a descriptive name, the key value, an optional expiration timestamp.",
    )

    class Config:
        env_prefix = "security_"
        env_file = ".env"


class ValidatorSettings(BaseSettings):
    check_format: bool = Field(
        True,
        description="Checks whether the email address has a valid structure."
        " Default value is True.",
    )
    check_blacklist: bool = Field(
        True,
        description="Checks the email address against a blacklist of domains."
        " Default value is True.",
    )
    check_dns: bool = Field(
        True, description="Checks the DNS MX-records. Default value is True."
    )
    dns_timeout: int = Field(
        10, description="Seconds until DNS timeout. Default value is 10 seconds.", gt=0
    )
    check_smtp: bool = Field(
        True,
        description="Checks whether the email address actually exists"
        " by initiating an SMTP conversation. Default value is True.",
    )
    smtp_timeout: int = Field(
        10, description="Seconds until SMTP timeout. Default value is 10 seconds.", gt=0
    )
    smtp_helo_host: Optional[str] = Field(
        description="Host name to use in SMTP HELO/EHLO. If set to None (the default),"
        " the fully qualified domain name of the local host is used."
    )
    smtp_from_address: Optional[str] = Field(
        description="Email address used for the sender in the SMTP conversation."
        " If set to None (the default), email addresses to be validated"
        " will be used as the senders as well."
    )
    smtp_skip_tls: bool = Field(
        False,
        description="Skips the TLS negotiation with the server, even when available."
        " Default value is False.",
    )
    smtp_debug: bool = Field(
        False,
        description="Activates smtplib's debug output which always goes to stderr."
        " Default value is False.",
    )

    class Config:
        env_prefix = "validator_"
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache validation results.
        allow_mutation = False
        frozen = True


class HealthCheckSettings(BaseSettings):
    ping_websites: Tuple[str, ...] = Field(
        ("https://www.google.com/", "https://duckduckgo.com/"),
        description="Websites which should be pinged to ensure"
        " an healthy internet connection is available."
        " Default websites are Google (https://www.google.com/)"
        " and DuckDuckGo (https://duckduckgo.com/).",
    )

    ping_timeout: int = Field(
        2,
        description="How many seconds should each website ping last before failing."
        " Default value is 2 seconds, timeout value should be greater than zero.",
        gt=0,
    )

    class Config:
        env_prefix = "health_check_"
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache health check dependency.
        allow_mutation = False
        frozen = True


class AppInsightsSettings(BaseSettings):
    instrumentation_key: Optional[UUID] = Field(
        None,
        description="Instrumentation Key provided by Azure Application Insights.",
        env="appinsights_instrumentationkey",
    )

    class Config:
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache logger dependency.
        allow_mutation = False
        frozen = True

    @property
    def connection_string(self):
        return f"InstrumentationKey={self.instrumentation_key}"


class ZeroBounceSettings(BaseSettings):
    api_key: Optional[str] = Field(
        None,
        description="API key used to interact with ZeroBounce.",
    )
    credits_threshold: int = Field(
        1,
        description="If ZeroBounce credits drop below given threshold,"
        " then the web service health check will fail.",
    )

    class Config:
        env_prefix = "zerobounce_"
        env_file = ".env"

        # Following configuration generates an hashable model,
        # which is required in order to cache validation results.
        allow_mutation = False
        frozen = True
