FROM python:3.10-slim-bullseye

# Prevents Python from writing pyc files to disc (equivalent to "python -B" option).
ENV PYTHONDONTWRITEBYTECODE=1

# Prevents Python from buffering stdout and stderr (equivalent to "python -u" option).
ENV PYTHONUNBUFFERED=1

# Skips the auto-update of built-in blacklist on startup.
# Blacklist is already downloaded during module installation.
ENV PY3VE_IGNORE_UPDATER=1

ENV PORT=8080 SSH_PORT=2222
EXPOSE 8080 2222

HEALTHCHECK --interval=30s --timeout=5s \
    CMD curl -f http://localhost:${PORT}/health || exit 1

WORKDIR /opt/app
RUN useradd -m app

# Create writable logs directory:
RUN mkdir -p ./logs
RUN chown -R app:app ./logs

# Install additional packages:
RUN apt-get update -qq && \
    export DEBIAN_FRONTEND=noninteractive && \
    apt-get install -y -qq --no-install-recommends curl dumb-init openssh-server && \
    rm -rf /var/lib/apt/lists/* /tmp/*

# Install Poetry:
ENV POETRY_HOME="/opt/poetry" \
    POETRY_VIRTUALENVS_IN_PROJECT="true"
RUN curl -sSL https://install.python-poetry.org | python3 && \
    ln -s ${POETRY_HOME}/bin/poetry /usr/local/bin/poetry

# Copy Azure App Service SSH configuration:
COPY ./docker/sshd_config /etc/ssh/

RUN \
    # Set root password for SSH access:
    echo "root:Docker!" | chpasswd && \
    # Create SSH keys:
    ssh-keygen -A && \
    # Make sure SSH run directory exists:
    mkdir -p /var/run/sshd

# Copy init scripts:
COPY ./docker/azure_appsvc_init.sh /usr/local/sbin/azure-appsvc-init
COPY ./docker/container_init.sh /usr/local/bin/container-init
RUN chmod 500 /usr/local/sbin/azure-appsvc-init 
RUN chmod 505 /usr/local/bin/container-init

# Restore dependencies:
COPY ./pyproject.toml ./poetry.lock ./
RUN poetry install --no-root --no-dev

# Copy project files:
COPY ./email_validator ./email_validator

# Run dumb-init as main process (https://github.com/Yelp/dumb-init):
ENTRYPOINT ["dumb-init", "--", "azure-appsvc-init"]

# Start gunicorn and SSH server:
CMD ["container-init"]
